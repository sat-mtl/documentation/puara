# Puara - website - documentation

## Intro

[Puara](https://github.com/puara) is a framework for building and deploying new media installations and New Interfaces for Music Expression (NIME).

Instrument designers (digital luthiers) can use the firmware modules to create their own controllers.
These controllers generate gestural data that can be sent to any computer, including the [Media Processing Unit (MPU)](https://github.com/Puara/MPU), designed for performance and new media deployment.

Puara is currently developed at the [Input Devices and Music Interaction Laboratory (IDMIL)](http://www-new.idmil.org/) and [Société des Arts Technologiques (SAT) - Metalab](https://sat.qc.ca/fr/recherche/metalab).

Some performances using the Puara Framework can be seen on YouTube:

- [live@CIRMMT Mini-Festival: Insomnia Rain](https://youtu.be/Ho9kRBwjyEQ)
- [live@CIRMMT Mini-Festival: The Turing Test](https://youtu.be/pFRPbD7EmvQ)
- [Freeze! for augmented drumkit (AMIWrist-Puara)](https://youtu.be/4jlAkBIFVPU)
- [Trouveur (Víctor Báez) - GuitarAMI version](https://youtu.be/lEWevEhnPPg)


*Puara* means "to tie" or "connect" in [Old Tupi](https://en.wikipedia.org/wiki/Tupi_language).

Puara Framework is currently hosted on GitHub: [https://github.com/puara](https://github.com/puara).

## Project content

This is the repository for building the [Puara docs website](https://sat-mtl.gitlab.io/documentation/puara). We also use this project to manage SAT contributions to the [main Puara group](https://github.com/puara).

Also, there are Puara MPU repositories available at SAT/Metalab's Gitlab: for the [Rapberry Pi](https://gitlab.com/sat-mtl/distribution/MPU) and for the [Nvidia Jetson](https://gitlab.com/sat-mtl/distribution/jetson-images).

The documentation website is based on [the template-website project](https://gitlab.com/sat-mtl/documentation/template-website).

## Contributing

We are currently working on identifying `good first issues` to improve the documentation. We welcome [bug reports](https://gitlab.com/sat-mtl/documentation/puara/-/issues) if you wish to help us improve documentation.

## License

The documentation is licensed under a [Creative Commons Attribution Non-Commercial ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/).

All the source code for this project repository (including examples and excerpts from the software) is licensed under the [GNU Public License version 3 (GPLv3)](https://www.gnu.org/licenses/gpl-3.0.en.html). Puara's licence information can be found at [the main project's website](https://github.com/puara).

## Code of Conduct

By participating in this project, you agree to abide by the [Code of Conduct](CODE_OF_CONDUCT.md). We expect all contributors to follow the [Code of Conduct](CODE_OF_CONDUCT.md) and to treat fellow humans with respect. It must be followed in all your interactions with the project.
