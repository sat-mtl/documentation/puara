Puara
======

.. container:: index-locales

   `En <../en/index.html>`__ -=- `Fr <../fr/index.html>`__

.. container:: index-intro

   Welcome to the Puara's documentation website
  
.. container:: index-menu

   `-` :doc:`/contents` - `GitHub <https://github.com/puara>`__ - `about us <https://sat.qc.ca/en/metalab>`__ -

.. container:: index-section

   What is Puara?
  
Puara is a framework for building and deploy new media installations and New Interfaces for Music Expression (NIME).

Instrument designers (digital luthiers) can use the firmware modules to create their own controllers. These controllers generate gestural data that can be sent to any computer, including the `Media processing Unit (MPU) <https://github.com/Puara/MPU>__`, designed for performance and mew media deployment.

Puara is currently developed at the `Société des Arts Technologiques (SAT) - Metalab <https://sat.qc.ca/>__` and the `Input Devices and Music Interaction Laboratory (IDMIL) <http://www.idmil.org/>`__.

.. container:: index-section

   Puara examples
  
Some performances using the Puara Framework can be seen on YouTube:

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/Ho9kRBwjyEQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

   <iframe width="560" height="315" src="https://www.youtube.com/embed/pFRPbD7EmvQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

   <iframe width="560" height="315" src="https://www.youtube.com/embed/4jlAkBIFVPU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

   <iframe width="560" height="315" src="https://www.youtube.com/embed/lEWevEhnPPg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

.. container:: index-section

   Why Puara?
  
Music, media, and technology research are intrinsically interdisciplinary.
From a research perspective, research involving the development of art-oriented tools usually lacks larger user bases for evaluation and data collection in production situations or real-life usage.
From the musical practice perspective, exploring state-of-art tools during their development allows artists to tailor these artifacts according to production needs while having the feedback needed to experiment with a little compromise on the quality of the artistic work.
Developing open-source tools such as Puara allows artists and researchers to deploy immersive spaces and set artistic performances using off-the-self plug-and-play hardware and software components.

Puara provides interconnection with standard artistic software/hardware for easy integration with the artist's workflow.
This integration allows artists to explore sound spatialization, video mapping, gestural control, and sound synthesis with embedded devices such as Raspberry Pi or Nvidia Jetson.
  
.. container:: index-section

   Get in touch

The code in this project is licensed under the MIT license, unless
otherwise specified within the file.

To know more about us, visit `Metalab <https://sat.qc.ca/en/recherche/metalab>`__.
  
.. container:: index-section

   Please show me the code!
   
For more information visit `the project repository <https://github.com/Puara>`__.
  
.. container:: index-section

   Sponsors

This project is made possible thanks to the `Society for Arts and Technology (SAT) <http://www.sat.qc.ca>`__ and the `Input Devices and Music Interaction Laboratory (IDMIL) <http://www.idmil.org/>`__
.
