Puara Module
============

The Puara Module is a library for creating music controllers using the Puara framework

It includes a Wi-Fi manager (set SSID/password for wireless networks), a
`OSC <https://en.wikipedia.org/wiki/Open_Sound_Control>`__ manager (set
addresses to send OSC messages), setup tools, and
`libmapper <http://libmapper.github.io/>`__ compatibility*.

This pseudo-library currently has the following dependencies:

-  `libmapper-arduino <https://github.com/puara/libmapper-arduino.git>`__
   (cloned from the `original
   library <https://github.com/mathiasbredholt/libmapper-arduino>`__).

How to use
----------

Use the template to start using the Puara Module.

.. toctree::
   :maxdepth: 1
   :caption: Module template:

   template



