Media processing Unit (MPU)
===========================

The Media Processing Unit, or MPU (former SPU) is the core of the
`Puara <https://github.com/Puara>`__ framework: it is responsible to
receive sensor data from all connected controllers, mapping devices, and
running embedded synthesis processes.

.. toctree::
   :maxdepth: 1
   :caption: MPU documentation:

   user_guide
   building_instructions_hardware
   building_instructions_os


