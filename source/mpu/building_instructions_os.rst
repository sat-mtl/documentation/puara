Building Instructions OS - Puara Media Processing Unit
======================================================

-  `Building Instructions - Puara Media Processing
   Unit <#building-instructions---puara-media-processing-unit>`__

   -  `BOM <#bom>`__
   -  `Prepare SD card <#prepare-sd-card>`__
   -  `First configuration <#first-configuration>`__
   -  `MPU Script <#mpu-script>`__

      -  `Setting the OS <#setting-the-os>`__
      -  `Set RealVNC security scheme <#set-realvnc-security-scheme>`__
      -  `Update OS, install basic apps, and install i3wm as an
         alternative window
         manager <#update-os-install-basic-apps-and-install-i3wm-as-an-alternative-window-manager>`__
      -  `Disable the built-in and HDMI
         audio <#disable-the-built-in-and-hdmi-audio>`__
      -  `Add Metalab’s MPA <#add-metalabs-mpa>`__
      -  `Configure AP <#configure-ap>`__
      -  `install Apache Guacamole <#install-apache-guacamole>`__
      -  `Install basic software <#install-basic-software>`__
      -  `Set Jack to start at boot <#set-jack-to-start-at-boot>`__
      -  `Set Pure Data systemd
         service <#set-pure-data-systemd-service>`__
      -  `Set SuperCollider systemd
         service <#set-supercollider-systemd-service>`__
      -  `Set up i3wm <#set-up-i3wm>`__
      -  `Adding a service to start JackTrip
         server <#adding-a-service-to-start-jacktrip-server>`__
      -  `Adding a service to start JackTrip
         client <#adding-a-service-to-start-jacktrip-client>`__
      -  `Make a systemd service for
         aj-snapshot <#make-a-systemd-service-for-aj-snapshot>`__
      -  `Install Samba server <#install-samba-server>`__
      -  `Make all systemd services
         available <#make-all-systemd-services-available>`__
      -  `Optimize boot time <#optimize-boot-time>`__
      -  `Mapping using jack in CLI <#mapping-using-jack-in-cli>`__
      -  `Latency tests <#latency-tests>`__
      -  `Jack available commands <#jack-available-commands>`__
      -  `Finish and rebooting <#finish-and-rebooting>`__

BOM
---

-  minimal hardware

   -  `Raspberry Pi 4 model
      B <https://www.raspberrypi.com/products/raspberry-pi-4-model-b/>`__
   -  External audio interface. If using the PiSound refer to
      https://blokas.io/pisound for installation instructions

-  software

   -  `Custom PREEMPT-RT kernel <RT_kernel.md>`__ (build on 5.10)
   -  `Raspberry Pi OS <https://ubuntu.com/download/raspberry-pi>`__
   -  Metalab software (through the `Metalab’s
      MPA <https://gitlab.com/sat-mtl/distribution/mpa-bullseye-arm64-rpi/>`__)

Prepare SD card
---------------

-  Download the (Raspberry Pi
   Imager)[https://www.raspberrypi.com/software/]
-  Flash the Raspberry Pi OS 64 bits into the microSD card. Use the
   configuration tool to set:

   -  Enable ssh
   -  set hostname: mpuXXX (replace XXX with the MPU’s ID)
   -  set user and password (mpu/mappings)
   -  insert WiFi credential if needed

-  OPTIONAL: if you plan to access your Rpi headlessly, you also need to
   ensure ssh is enabled on first boot by navigating to the ``boot``
   partition on the microSD card and creating an empty file called
   **ssh** (e.g., use ``touch ssh`` in the command line)
-  Insert the microSD card in the Rpi and turn it on

First configuration
-------------------

-  First configuration for the Raspberry Pi. Information on available
   **raspi-config no interactive** commands can be found at
   https://github.com/raspberrypi-ui/rc_gui/blob/master/src/rc_gui.c

-  Ssh (with the X11-Forwarding flag) to the Rpi:
   ``ssh -X mpu@<ip_address>`` or ``ssh -X mpu@mpuXXX.local``. The Rpi
   might take a longer time to be available to ssh during the first boot
   as it is expanding the filesystem. Obs: it is important to enable X11
   forwarding as the installed SuperCollider version needs it to run
   sclang and install SATIE.

-  Copy and paste the code block below to automatically run the `MPU
   Script <#mpu-script>`__ routine:

.. code:: bash

   sudo apt install -y tmux git liblo-tools
   mkdir ~/sources && cd ~/sources &&\
   git clone https://github.com/Puara/MPU.git &&\
   cd ~/sources/MPU/scripts &&\
   sudo chmod +x building_script.sh rename_mpu.sh change_ipblock.sh &&\
   ./building_script.sh &&\
   ./run_script.sh

-  You will be asked to choose an MPU ID before the script starts
   preparing the OS.

-  The next section describes all steps automatically executed by the
   commands presented above. If you copied and pasted the commands
   above, you don’t need to follow the following instructions and your
   MPU will be automatically set.

MPU Script
----------

Setting the OS
~~~~~~~~~~~~~~

.. code:: bash

   sudo raspi-config nonint do_vnc 0
   sudo raspi-config nonint do_vnc_resolution 1920x1080
   sudo raspi-config nonint do_memory_split 256
   sudo raspi-config nonint do_wifi_country CA
   sudo raspi-config --expand-rootfs

Set RealVNC security scheme
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

   echo -e "mappings\nmappings" | sudo vncpasswd -service
   sudo sed -i '$a Authentication=VncAuth' /root/.vnc/config.d/vncserver-x11

-  The password set by default is ``mappings``

Update OS, install basic apps, and install i3wm as an alternative window manager
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

   sudo apt update -y && sudo apt upgrade -y
   sudo apt install -y i3 i3blocks htop vim feh x11-utils git-lfs fonts-font-awesome jacktrip

-  Update desktop alternatives and select i3 as the default window
   manager:

.. code:: bash

   sudo update-alternatives --install /usr/bin/x-session-manager x-session-manager /usr/bin/i3 60
   echo "2" | sudo update-alternatives --config x-session-manager

Disable the built-in and HDMI audio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

   sudo sed -i -e 's/Enable audio (loads snd_bcm2835)/Disable audio (snd_bcm2835)/ ; s/dtparam=audio=on/dtparam=audio=off/ ; s/dtoverlay=vc4-kms-v3d/dtoverlay=vc4-kms-v3d,noaudio/' /boot/config.txt

Add Metalab’s MPA
~~~~~~~~~~~~~~~~~

.. code:: bash

   sudo apt install -y coreutils wget && \
   wget -qO - https://sat-mtl.gitlab.io/distribution/mpa-bullseye-arm64-rpi/sat-metalab-mpa-keyring.gpg \
       | gpg --dearmor \
       | sudo dd of=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg && \
   echo 'deb [ arch=arm64, signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-bullseye-arm64-rpi/debs/ sat-metalab main' \
       | sudo tee /etc/apt/sources.list.d/sat-metalab-mpa.list && \
   sudo apt update &&\
   sudo apt upgrade -y

Configure AP
~~~~~~~~~~~~

-  Install dependencies:

.. code:: bash

   sudo apt install -y dnsmasq hostapd

-  Create the hostapd config file:

.. code:: bash

   cat <<- "EOF" | sudo tee /etc/hostapd/hostapd.conf
   interface=wlan0
   driver=nl80211
   ssid=MPUXXX
   hw_mode=g
   channel=6
   macaddr_acl=0
   auth_algs=1
   ignore_broadcast_ssid=0
   wpa=2
   wpa_passphrase=mappings
   wpa_key_mgmt=WPA-PSK
   wpa_pairwise=TKIP
   rsn_pairwise=CCMP
   EOF

-  Configure a static IP for the wlan0 interface:

.. code:: bash

   sudo sed -i -e 's/hostname/mpuXXX/' -e '$a\\ninterface wlan0\n    static ip_address=192.168.5.1/24\n    nohook wpa_supplicant\n    #denyinterfaces eth0\n    #denyinterfaces wlan0\n' /etc/dhcpcd.conf

-  Set hostapd to read the config file:

.. code:: bash

   sudo sed -i 's,#DAEMON_CONF="",DAEMON_CONF="/etc/hostapd/hostapd.conf",' /etc/default/hostapd

-  Start the hostapd service:

.. code:: bash

   sudo systemctl unmask hostapd &&
   sudo systemctl enable hostapd &&
   sudo systemctl start hostapd
   sudo DEBIAN_FRONTEND=noninteractive apt install -y netfilter-persistent iptables-persistent

-  Start configuring the dnsmasq service

.. code:: bash

   sudo unlink /etc/resolv.conf &&
   echo nameserver 8.8.8.8 | sudo tee /etc/resolv.conf &&
   cat <<- "EOF" | sudo tee /etc/dnsmasq.conf
   interface=wlan0
   dhcp-range=192.168.5.2,192.168.5.20,255.255.255.0,24h
   EOF

-  Modify ``/lib/systemd/system/dnsmasq.service`` to launch after
   network get ready:

.. code:: bash

   sudo sed -i '0,/^\s*$/'\
   's//After=network-online.target\nWants=network-online.target\n/' \
   /lib/systemd/system/dnsmasq.service

-  To prevent a long waiting time during boot, edit
   ``/lib/systemd/system/systemd-networkd-wait-online.service``:

.. code:: bash

   sudo sed -i '\,ExecStart=/lib/systemd/systemd-networkd-wait-online, s,$, --any,' /lib/systemd/system/systemd-networkd-wait-online.service
   sudo systemctl daemon-reload

install Apache Guacamole
~~~~~~~~~~~~~~~~~~~~~~~~

-  More info at https://guacamole.apache.org

-  Reference: `Guacamole
   manual <https://guacamole.apache.org/doc/gug/>`__

-  Install dependencies:

.. code:: bash

   sudo apt install -y libcairo2-dev libpng-dev libjpeg62-turbo-dev libtool-bin libossp-uuid-dev libavcodec-dev libavformat-dev libavutil-dev libswscale-dev freerdp2-dev libpango1.0-dev libssh2-1-dev libtelnet-dev libvncserver-dev libwebsockets-dev libpulse-dev libssl-dev libvorbis-dev libwebp-dev jetty9

-  Clone git and install (set for version 1.4.0):

.. code:: bash

   cd ~/sources
   wget -O guacamole-server-1.4.0.tar.gz "https://apache.org/dyn/closer.lua/guacamole/1.4.0/source/guacamole-server-1.4.0.tar.gz?action=download"
   tar -xzf guacamole-server-1.4.0.tar.gz
   cd guacamole-server-1.4.0
   autoreconf -fi
   ./configure
   make
   sudo make install
   sudo ldconfig
   cd ~/sources
   wget -O guacamole.war "https://apache.org/dyn/closer.lua/guacamole/1.4.0/binary/guacamole-1.4.0.war?action=download"
   sudo cp ~/sources/guacamole.war /var/lib/jetty9/webapps/guacamole.war

-  Create the Guacamole home:

.. code:: bash

   sudo mkdir /etc/guacamole

-  Create ``user-mapping.xml``

.. code:: bash

   cat <<- "EOF" | sudo tee /etc/guacamole/user-mapping.xml
   <user-mapping>
       <authorize
       username="mpu"
       password="mappings">
           <connection name="MPU">
           <protocol>vnc</protocol>
           <param name="hostname">localhost</param>
           <param name="port">5900</param>
           <param name="password">mappings</param>
           </connection>
       </authorize>
   </user-mapping>
   EOF

.. code:: bash

   sudo mv /var/lib/jetty9/webapps/root /var/lib/jetty9/webapps/root-OLD
   sudo mv /var/lib/jetty9/webapps/guacamole.war /var/lib/jetty9/webapps/root.war

-  Change Guacamole login screen:

.. code:: bash

   sudo mkdir /etc/guacamole/extensions
   sudo cp ~/sources/MPU/mpu.jar /etc/guacamole/extensions/mpu.jar

-  Configure addresses

.. code:: bash

   sudo apt install apache2 -y
   sudo a2enmod proxy
   sudo a2enmod proxy_http
   sudo a2enmod proxy_balancer
   sudo a2enmod lbmethod_byrequests
   sudo a2enmod rewrite
   sudo mv /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/000-default.conf.bak

.. code:: bash

   cat <<- "EOF" | sudo tee /etc/apache2/sites-available/000-default.conf
   <VirtualHost *:80>

       RewriteEngine on
       RewriteRule ^/webmapper$ /webmapper/ [R]

       ProxyRequests Off
       ProxyPreserveHost On

       ProxyPass /webmapper http://127.0.0.1:50000
       ProxyPassReverse /webmapper http://127.0.0.1:50000
       ProxyPass / http://127.0.0.1:8080/
       ProxyPassReverse / http://127.0.0.1:8080/

   </VirtualHost>
   EOF

.. code:: bash

   sudo systemctl restart apache2

-  Set guacd to start at boot

.. code:: bash

   cat <<- "EOF" | sudo tee /lib/systemd/system/guacd.service
   [Unit]
   Description=Run Guacamole server
   After=multi-user.target

   [Service]
   Type=idle
   Restart=always
   User=mpu
   ExecStart=/usr/local/sbin/guacd -b 127.0.0.1 -f

   [Install]
   WantedBy=default.target
   EOF
   sudo systemctl daemon-reload
   sudo systemctl enable guacd
   sudo systemctl start guacd

Install basic software
~~~~~~~~~~~~~~~~~~~~~~

-  Installing SuperCollider, SC3-Plugins, jackd2 (if needed):

.. code:: bash

   sudo apt install -y supercollider sc3-plugins libmapper python3-netifaces webmapper jackd2 qjackctl puredata aj-snapshot

-  Installing SATIE:

.. code:: bash

   cd ~/sources
   git clone https://gitlab.com/sat-mtl/tools/satie/satie.git
   echo "Quarks.install(\"SC-HOA\");Quarks.install(\"~/sources/satie\")" | sclang

Set Jack to start at boot
~~~~~~~~~~~~~~~~~~~~~~~~~

-  Add a dbus security policy:

.. code:: bash

   cat <<- "EOF" | sudo tee /etc/dbus-1/system.conf
   <policy user="micah">
        <allow own="org.freedesktop.ReserveDevice1.Audio0"/>
        <allow own="org.freedesktop.ReserveDevice1.Audio1"/>
   </policy>
   EOF

-  Add path to environment:

.. code:: bash

   cat <<- "EOF" | sudo tee /etc/environment
   export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/dbus/system_bus_socket
   EOF

-  Create the systemd service file:

.. code:: bash

   cat <<- "EOF" | sudo tee /lib/systemd/system/jackaudio.service
   [Unit]
   Description=JACK Audio
   After=sound.target

   [Service]
   LimitRTPRIO=infinity
   LimitMEMLOCK=infinity
   User=mpu
   Environment="JACK_NO_AUDIO_RESERVATION=1"
   ExecStart=/usr/bin/jackd -R -P95 -t2000 -dalsa -dhw:1 -p256 -n2 -r48000 -s &

   [Install]
   WantedBy=multi-user.target
   EOF
   sudo systemctl daemon-reload
   sudo systemctl enable jackaudio.service

-  Some commands:

-  List information and connections on ports: ``jack_lsp -c``

-  Connect ports:
   ``jack_connect [ -s | --server servername ] [-h | --help ] port1 port2``
   (The exit status is zero if successful, 1 otherwise)

-  Disconnect ports:
   ``jack_disconnect [ -s | --server servername ] [ -h | --help ] port1 port2``

Set Pure Data systemd service
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

   cat <<- "EOF" | sudo tee /lib/systemd/system/puredata.service
   [Unit]
   Description=Pure Data
   After=sound.target jackaudio.service
   ConditionPathExists=/home/mpu/Documents/default.pd

   [Service]
   LimitRTPRIO=infinity
   LimitMEMLOCK=infinity
   User=mpu
   ExecStart=pd -nogui -noprefs -rt -jack -inchannels 2 -outchannels 2 ~/Documents/default.pd

   [Install]
   WantedBy=multi-user.target
   EOF
   sudo systemctl daemon-reload
   sudo systemctl enable puredata.service
   sudo systemctl start puredata.service

Set SuperCollider systemd service
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  OBS: starting as a user service to allow required access

.. code:: bash

   mkdir -p ~/.config/systemd/user
   cat <<- "EOF" | tee ~/.config/systemd/user/supercollider.service
   [Unit]
   Description=SuperCollider
   After=multi-user.target
   ConditionPathExists=/home/mpu/Documents/default.scd

   [Service]
   Type=idle
   Restart=always
   ExecStart=sclang -D /home/mpu/Documents/default.scd

   [Install]
   WantedBy=default.target
   EOF
   sudo chmod 644 ~/.config/systemd/user/supercollider.service
   systemctl --user daemon-reload
   systemctl --user enable --now supercollider.service

Set up i3wm
~~~~~~~~~~~

-  Copy i3_config to ``~/.config/i3`` and rename to ``config``:
-  Copy i3status.conf to ``/etc``:

.. code:: bash

   mkdir ~/.config/i3
   cp ~/sources/MPU/i3_config ~/.config/i3/config
   sudo cp ~/sources/MPU/i3status.conf /etc/i3status.conf
   cp ~/sources/MPU/wallpaper.png ~/Pictures/wallpaper.png
   sudo sed -i -e "s/MPU/MPUXXX/" /etc/i3status.conf

-  OBS: for checking Font Awesome: https://fontawesome.com/v5/cheatsheet

Adding a service to start JackTrip server
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  OBS: client name is the name of the other machine

.. code:: bash

   cat <<- "EOF" | sudo tee /lib/systemd/system/jacktrip_server.service
   [Unit]
   Description=Run JackTrip server
   After=multi-user.target

   [Service]
   Type=idle
   Restart=always
   ExecStart=~/sources/jacktrip/builddir/jacktrip -s --clientname jacktrip_client

   [Install]
   WantedBy=default.target
   EOF

-  To enable the service at boot:
   ``sudo systemctl enable jacktrip_server.service``

Adding a service to start JackTrip client
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Replace the IP address for the server IP.

.. code:: bash

   cat <<- "EOF" | sudo tee /lib/systemd/system/jacktrip_client.service
   [Unit]
   Description=Run JackTrip client
   After=multi-user.target

   [Service]
   Type=idle
   Restart=always
   ExecStart=~/sources/jacktrip/builddir/jacktrip -c 192.168.5.1 --clientname jacktrip_client

   [Install]
   WantedBy=default.target
   EOF

-  If you want to enable the client, disable the service and run
   ``sudo systemctl enable jacktrip_client.service``
-  To manually use as a client with IP address:
   ``./jacktrip -c [xxx.xx.xxx.xxx]``, or with name:
   ``./jacktrip -c mpuXXX.local``

Make a systemd service for aj-snapshot
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  More info at http://aj-snapshot.sourceforge.net/

-  To create a snapshot:
   ``aj-snapshot -f ~/Documents/default.connections``

-  To remove all Jack connections: ``aj-snapshot -xj``

-  To save connections:
   ``sudo aj-snapshot -f ~/Documents/default.connections``

-  To restore connections:
   ``sudo aj-snapshot -r ~/Documents/default.connections``

-  Set custom Jack connections to load at boot:

.. code:: bash

   cat <<- "EOF" | sudo tee /lib/systemd/system/ajsnapshot.service
   [Unit]
   Description=AJ-Snapshot
   After=sound.target jackaudio.service
   ConditionPathExists=/home/mpu/Documents/default.connections

   [Service]
   Type=idle
   Restart=always
   User=mpu
   ExecStart=/usr/local/bin/aj-snapshot -d ~/Documents/default.connections

   [Install]
   WantedBy=multi-user.target
   EOF
   sudo systemctl daemon-reload
   sudo systemctl enable ajsnapshot.service
   sudo systemctl start ajsnapshot.service

-  If you want to save a ajsnapshot file run:
   ``aj-snapshot -f ~/Documents/default.connections``

Install Samba server
~~~~~~~~~~~~~~~~~~~~

-  OBS: Choose *No* if asked **Modify smb.conf to use WINS settings from
   DHCP?**

.. code:: bash

   sudo apt install -y samba
   sudo systemctl stop smbd.service
   sudo mv /etc/samba/smb.conf /etc/samba/smb.conf.orig

-  Create Samba configuration file and set the user:

.. code:: bash

   cat <<- "EOF" | sudo tee /etc/samba/smb.conf
   [global]
           server string = MPUXXX
           server role = standalone server
           interfaces = lo eth0 wlan0
           bind interfaces only = no
           smb ports = 445
           log file = /var/log/samba/smb.log
           max log size = 10000
           map to guest = bad user

   [Documents]
           path = "/home/mpu/Documents"
           read only = no
           browsable = yes
           valid users = mpu
           vfs objects = recycle
           recycle:repository = .recycle
           recycle:touch = Yes
           recycle:keeptree = Yes
           recycle:versions = Yes
           recycle:noversions = *.tmp,*.temp,*.o,*.obj,*.TMP,*.TEMP
           recycle:exclude = *.tmp,*.temp,*.o,*.obj,*.TMP,*.TEMP
           recycle:excludedir = /.recycle,/tmp,/temp,/TMP,/TEMP

   EOF
   (echo mappings; echo mappings) | sudo smbpasswd -a mpu
   sudo smbpasswd -e mpu
   sudo systemctl start smbd.service
   sudo systemctl enable smbd.service

Make all systemd services available
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

   sudo systemctl daemon-reload

Optimize boot time
~~~~~~~~~~~~~~~~~~

-  Disable bluetooth and hciuart:

.. code:: bash

   sudo sed  -i -e '$a\\n# Disable bluetooth (fast boot time)\ndtoverlay=disable-bt' /boot/config.txt
   sudo systemctl disable hciuart.service

-  Disable Modem Manager service. If using radio communication (2g, 3g,
   4g) or USB devices that use radio, re-enabling it might be needed

.. code:: bash

   sudo systemctl disable ModemManager.service

-  Disable Samba NetBIOS name server daemon. If NETBIOS is needed,
   re-enable it

.. code:: bash

   sudo systemctl disable nmbd.service

Mapping using jack in CLI
~~~~~~~~~~~~~~~~~~~~~~~~~

-  Check available devices: ``cat /proc/asound/cards``. If you have
   multiple devices available, can call them by name
-  Lists jack available ports: ``jack_lsp``
-  List information and connections on ports: ``jack_lsp -c`` or
   ``jack_lsp -A``
-  Connect ports:
   ``jack_connect [ -s | --server servername ] [-h | --help ] port1 port2``
   (The exit status is zero if successful, 1 otherwise)
-  Disconnect ports:
   ``jack_disconnect [ -s | --server servername ] [ -h | --help ] port1 port2``

Latency tests
~~~~~~~~~~~~~

-  Make sure JackTrip is running.

-  Connect the necessary audio cable to create a loopback on the
   client’s audio interface (audio OUT -> audio IN)

-  For the loopback (same interface test):
   ``jack_delay -I system:capture_2 -O system:playback_2``

-  run the test:
   ``jack_delay -O jacktrip_client.local:send_2 -I jacktrip_client.local:receive_2``

Jack available commands
~~~~~~~~~~~~~~~~~~~~~~~

-  To get a list on the computer, type **jack** and hit *Tab*

+----------+------------+-----------------+------------+--------------+
| command  | command    | command         | command    | command      |
+==========+============+=================+============+==============+
| ja       | ja         | jack_capture    | jack_c     | jack_connect |
| ck_alias | ck_bufsize |                 | apture_gui |              |
+----------+------------+-----------------+------------+--------------+
| jackdbus | jack_      | jack-dl         | jack       | jack_evmon   |
|          | disconnect |                 | -dssi-host |              |
+----------+------------+-----------------+------------+--------------+
| j        | jack_lsp   | jack_metro      | jack       | jack_midi_   |
| ack_load |            |                 | _midi_dump | latency_test |
+----------+------------+-----------------+------------+--------------+
| jack_ne  | jack       | jack_netsource  | jack-osc   | jack-play    |
| t_master | _net_slave |                 |            |              |
+----------+------------+-----------------+------------+--------------+
| jack_sa  | jack-scope | jack            | jack_sess  | j            |
| mplerate |            | _server_control | ion_notify | ack_showtime |
+----------+------------+-----------------+------------+--------------+
| j        | jack       | jack-transport  | jack-udp   | jack_unload  |
| ack_thru | _transport |                 |            |              |
+----------+------------+-----------------+------------+--------------+
| jack     | jack_cpu   | jack_cpu_load   | jackd      | jack_wait    |
| _control |            |                 |            |              |
+----------+------------+-----------------+------------+--------------+
| jack_f   | ja         | jack-keyboard   | jack_lat   | jack_midiseq |
| reewheel | ck_iodelay |                 | ent_client |              |
+----------+------------+-----------------+------------+--------------+
| jack_    | jack_moni  | jack            | jac        |              |
| midisine | tor_client | _multiple_metro | k-plumbing |              |
+----------+------------+-----------------+------------+--------------+
| j        | jack_rec   | jack-record     | jack_test  |              |
| ack-rack |            |                 |            |              |
+----------+------------+-----------------+------------+--------------+
| jack_s   | jack_sim   | jack_simple     | j          |              |
| imdtests | ple_client | _session_client | ack_zombie |              |
+----------+------------+-----------------+------------+--------------+

-  To check Jack logs: ``sudo journalctl -u jack.service``

Finish and rebooting
~~~~~~~~~~~~~~~~~~~~

.. code:: bash

   echo "Build done!"
   echo
   echo "rebooting..."
   echo
   sudo reboot

Alternatively, you can download the MPU OS image to use in any Raspberry
Pi. There’s no need to reproduce the hardware of the MPU.

The image can be downloaded in the
`Releases <https://gitlab.com/sat-mtl/distribution/rpi-images/-/releases>`__
section](https://gitlab.com/sat-mtl/distribution/rpi-images/-/releases)
of the `SAT/Metalab <https://sat.qc.ca/fr/recherche/metalab>`__
maintained `project
repository <https://gitlab.com/sat-mtl/distribution/rpi-images>`__.
