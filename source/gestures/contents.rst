Puara Gestures
==============

Puara-Gestures is a library to provide high-level gestural descriptor functions for the Puara Framework.

The library can be loaded into any project. An example of application can be seen in the `GuitarAMI firmware repository <https://github.com/Puara/GuitarAMI>`__.
